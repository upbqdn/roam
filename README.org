#+title: Hello
#+EXPORT_FILE_NAME: about

I'm Marek. Welcome to the public repository of my notes. Please note that this
website currently contains only a small fraction of my notes, and some are
incomplete. Hopefully, there will be a time when I can add more.

You can contact me at [[mailto:hi@marek.onl][hi@marek.onl]].

:PROPERTIES:
:ID:       a1e0390c-d97c-45cf-993d-4db4ea99332f
:END:
#+title: Test Node
#+hugo_tags:
#+bibliography: ref.bib
#+cite_export: csl

#+BEGIN_section
<<.df-1>> *[[.df-1][Definition 1]].* Let $A_1, \ldots, A_n$ be sets. Then the
*Cartesian product* $A_1 \times \cdots \times A_n$ is the set of all ordered \(n\)-tuples
$\left( a_1, \ldots, a_n \right)$ such that $a_i \in A_i$ for $1 \leq i \leq n$.
#+END_section

As per [[.theorem-1][Theorem 1]]. [cite:check @rijndael; @aumasson]

This node references the [[id:a1e0390c-d97c-45cf-993d-4db4ea99332f][Initial Node]].
#+CAPTION: foobar
#+BEGIN_section
<<.theorem-1>> *[[.theorem-1][Theorem 1]]*
/If $a^2=b$ and \( b=2 \), then the solution must be either $$ a=+\sqrt{2} $$ or
\[ a=-\sqrt{2}. \]/
#+END_section

#+macro: yt @@html:<iframe id="vid" src="https://www.youtube-nocookie.com/embed/$1" allowfullscreen></iframe>@@


Video

#+attr_html: :controls t
#+begin_video
<source src="/data/mining.mp4" type="video/mp4">
#+end_video

Here's  another one:

@@html:
<iframe src="/data/BTC.html"
        class="plot" allowfullscreen>
</iframe>
@@

@@html:
<iframe src="https://www.youtube-nocookie.com/embed/XqZsoesa55w"
        id="vid" allowfullscreen>
</iframe>
@@

As per [[.theorem-1][Theorem 1]].
This is weird.
\\
\\

# CAPTION: syzygy.
#+NAME: foo
<<.snip-3>>
#+begin_src rust -n
fn main() -> Foo {
    println!("ffoofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoof");
}
#+end_src
#+begin_center
*[[.snip-3][/Snippet/ 3]]:* /hello world./
#+end_center
\\

#+BEGIN_section
<<.theorem-1>> *[[.theorem-1][Theorem 1]]*
/If $a^2=b$ and \( b=2 \), then the solution must be either $$ a=+\sqrt{2} $$ or
\[ a=-\sqrt{2}. \]/
#+END_section

* Subtitle
** Subsubtitle
#+BEGIN_section
<<.df-1>> *[[.df-1][Definition 1]].* Let $A_1, \ldots, A_n$ be sets. Then the
*Cartesian product* $A_1 \times \cdots \times A_n$ is the set of all ordered \(n\)-tuples
$\left( a_1, \ldots, a_n \right)$ such that $a_i \in A_i$ for $1 \leq i \leq n$.
#+END_section

As per [[.theorem-1][Theorem 1]].

This node references the [[id:a1e0390c-d97c-45cf-993d-4db4ea99332f][Initial Node]].
#+BEGIN_section
<<.theorem-1>> *[[.theorem-1][Theorem 1]]*
/If $a^2=b$ and \( b=2 \), then the solution must be either $$ a=+\sqrt{2} $$ or
\[ a=-\sqrt{2}. \]/
#+END_section

As per [[.theorem-1][Theorem 1]].

#+BEGIN_section
<<.theorem-1>> *[[.theorem-1][Theorem 1]]*
/If $a^2=b$ and \( b=2 \), then the solution must be either $$ a=+\sqrt{2} $$ or
\[ a=-\sqrt{2}. \]/
#+END_section

* foo
** bar
#+BEGIN_section
<<.df-1>> *[[.df-1][Definition 1]].* Let $A_1, \ldots, A_n$ be sets. Then the
*Cartesian product* $A_1 \times \cdots \times A_n$ is the set of all ordered \(n\)-tuples
$\left( a_1, \ldots, a_n \right)$ such that $a_i \in A_i$ for $1 \leq i \leq n$.
#+END_section

As per [[.theorem-1][Theorem 1]].

This node references the [[id:a1e0390c-d97c-45cf-993d-4db4ea99332f][Initial Node]].
#+BEGIN_section
<<.theorem-1>> *[[.theorem-1][Theorem 1]]*
/If $a^2=b$ and \( b=2 \), then the solution must be either $$ a=+\sqrt{2} $$ or
\[ a=-\sqrt{2}. \]/
#+END_section

As per [[.theorem-1][Theorem 1]].

#+BEGIN_section
<<.theorem-1>> *[[.theorem-1][Theorem 1]]*
/If $a^2=b$ and \( b=2 \), then the solution must be either $$ a=+\sqrt{2} $$ or
\[ a=-\sqrt{2}. \]/
#+END_section

* foofoo
** vbar

{{{yt(XqZsoesa55w)}}}

Here's a lovely plot:

@@html:
<iframe src="/data/BTC.html"
        class="plot" allowfullscreen>
</iframe>
@@

#+BEGIN_section
<<.df-1>> *[[.df-1][Definition 1]].* Let $A_1, \ldots, A_n$ be sets. Then the
*Cartesian product* $A_1 \times \cdots \times A_n$ is the set of all ordered \(n\)-tuples
$\left( a_1, \ldots, a_n \right)$ such that $a_i \in A_i$ for $1 \leq i \leq n$.
#+END_section

As per [[.theorem-1][Theorem 1]].

This node references the [[id:a1e0390c-d97c-45cf-993d-4db4ea99332f][Initial Node]].
#+BEGIN_section
<<.theorem-1>> *[[.theorem-1][Theorem 1]]*
/If $a^2=b$ and \( b=2 \), then the solution must be either $$ a=+\sqrt{2} $$ or
\[ a=-\sqrt{2}. \]/
#+END_section

As per [[.theorem-1][Theorem 1]].

#+BEGIN_section
<<.theorem-1>> *[[.theorem-1][Theorem 1]]*
/If $a^2=b$ and \( b=2 \), then the solution must be either $$ a=+\sqrt{2} $$ or
\[ a=-\sqrt{2}. \]/
#+END_section

#+print_bibliography:
